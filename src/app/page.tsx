import Link from "next/link";

export default function Home() {
    return (
        <Link href="/demo">Demo React / HTML / CSS</Link>
    )
}
