import type {Metadata} from 'next'
import Link from "next/link";

export const metadata: Metadata = {
    title: 'Demo React HTML CSS',
}

export default function DemoLayout({children,}: { children: React.ReactNode }) {
    return (
        <div>
            <Link href="/demo">Demo Home</Link>
            <h6>------</h6>
            <div>
                {children}
            </div>
        </div>
    )
}
