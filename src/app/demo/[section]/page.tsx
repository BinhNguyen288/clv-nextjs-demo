export default async function Page({params,}: { params: { section: string } }) {
    switch (params.section) {
        case 'html':
            return SectionHtml()
        default:
            return Missing()
    }
}

function Missing() {
    return <h1>404 - Missing demo section</h1>
}

function SectionHtml() {
    return <div>
        <h6>HTML test</h6>
        <p>Paragraph</p>
    </div>
}