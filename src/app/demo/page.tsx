import Link from "next/link";

const sections = ["html", "html-with-css", "react-components"]

export default function DemoPage() {
    return (
        <ul>
            {sections.map((section) => (
                <li key={section}>
                    <Link href={`/demo/${section}`}>Demo {section}</Link>
                </li>
            ))}
        </ul>
    )
}
